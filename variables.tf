variable "gcp_project_id" {
    type = string
}

variable "gcp_zone" {
    default = "us-central1-a"
}

variable "gcp_cluster_name" {
    default = "standard-cluster-2"
}
