provider "kubernetes" {
  version = "1.10.0-custom"
  host = "${google_container_cluster.primary.endpoint}"
  cluster_ca_certificate = "${base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)}"
}

resource "kubernetes_secret" "google-application-credentials" {
  metadata {
    name = "google-application-credentials"
  }
  data = {
    credentials = "${base64decode(google_service_account_key.mykey.private_key)}"
  }
}

resource "kubernetes_deployment" "demo_deployment_hyperflow" {
  metadata {
    name = "demo-deployment-hyperflow"
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        name = "demo-deployment-hyperflow"
      }
    }

    template {
      metadata {
        labels = {
          name = "demo-deployment-hyperflow"
        }
      }

      spec {
        volume {
          name = "my-pvc-nfs"

          persistent_volume_claim {
            claim_name = "nfs"
          }
        }

        volume {
          name = "secret-volume"

          secret {
            secret_name = "google-application-credentials"
          }
        }

        container {
          name    = "example"
          image   = "koxu1996/hyperflow"
          command = ["sleep", "infinity"]

          env {
            name  = "GOOGLE_APPLICATION_CREDENTIALS"
            value = "/etc/secret-volume/credentials"
          }

          env {
            name  = "GCP_ZONE"
            value = var.gcp_zone
          }

          env {
            name  = "GCP_CLUSTER_NAME"
            value = var.gcp_cluster_name
          }

          volume_mount {
            name       = "my-pvc-nfs"
            mount_path = "/opt/hfstorage"
          }

          volume_mount {
            name       = "secret-volume"
            mount_path = "/etc/secret-volume"
          }

          image_pull_policy = "Always"
        }
      }
    }
  }
}

resource "kubernetes_deployment" "demo_deployment_redis" {
  metadata {
    name = "demo-deployment-redis"
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        name = "demo-deployment-redis"
      }
    }

    template {
      metadata {
        labels = {
          name = "demo-deployment-redis"
        }
      }

      spec {
        container {
          name  = "redis"
          image = "redis"
        }
      }
    }
  }
}

resource "kubernetes_service" "redis_service" {
  metadata {
    name = "redis-service"
  }

  spec {
    port {
      port = 6379
    }

    selector = {
      name = "demo-deployment-redis"
    }
  }
}

resource "kubernetes_deployment" "nfs_server" {
  metadata {
    name = "nfs-server"
  }

  depends_on  = ["google_compute_disk.default"]

  spec {
    replicas = 1

    selector {
      match_labels = {
        role = "nfs-server"
      }
    }

    template {
      metadata {
        labels = {
          role = "nfs-server"
        }
      }

      spec {
        volume {
          name = "mypvc"

          gce_persistent_disk {
            pd_name = "hf-storage"
            fs_type = "ext4"
          }
        }

        container {
          name  = "nfs-server"
          image = "gcr.io/google_containers/volume-nfs:0.8"

          port {
            name           = "nfs"
            container_port = 2049
          }

          port {
            name           = "mountd"
            container_port = 20048
          }

          port {
            name           = "rpcbind"
            container_port = 111
          }

          volume_mount {
            name       = "mypvc"
            mount_path = "/exports"
          }

          security_context {
            privileged = true
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "nfs_server" {
  metadata {
    name = "nfs-server"
  }

  spec {
    port {
      name = "nfs"
      port = 2049
    }

    port {
      name = "mountd"
      port = 20048
    }

    port {
      name = "rpcbind"
      port = 111
    }

    selector = {
      role = "nfs-server"
    }
  }
}

resource "kubernetes_persistent_volume" "nfs" {
  metadata {
    name = "nfs"
  }

  spec {
    capacity = {
      storage = "1Gi"
    }

    access_modes = ["ReadWriteMany"]

    persistent_volume_source {
      nfs {
        path   = "/"
        server = "nfs-server.default.svc.cluster.local"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "nfs" {
  metadata {
    name = "nfs"
  }

  spec {
    access_modes = ["ReadWriteMany"]

    storage_class_name = ""

    resources {
      requests = {
        storage = "1Gi"
      }
    }
  }
}
