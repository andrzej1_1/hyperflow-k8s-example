resource "google_service_account" "service_account" {
  account_id   = "my-service-account"
  display_name = "Service Account"
}

resource "google_project_iam_binding" "admin-account-gke-iam" {
  role               = "roles/container.admin"

  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "admin-account-storage-iam" {
  role               = "roles/storage.admin"

  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_service_account_key" "mykey" {
  service_account_id = google_service_account.service_account.name
  public_key_type    = "TYPE_X509_PEM_FILE"
}

provider "google" {
  project     = var.gcp_project_id
}

resource "google_project_service" "kubernetes" {
  project = var.gcp_project_id
  service = "container.googleapis.com"
}

resource "google_container_cluster" "primary" {
  name        = var.gcp_cluster_name
  depends_on  = ["google_project_service.kubernetes"]
  location    = var.gcp_zone

  initial_node_count = 3

  node_config {
    machine_type = "e2-small"
  }
}

resource "google_project_service" "compute_engine" {
  project = var.gcp_project_id
  service = "compute.googleapis.com"
}

resource "google_compute_disk" "default" {
  name  = "hf-storage"
  depends_on  = ["google_project_service.compute_engine"]
  size  =  "1"
  type  = "pd-ssd"
  zone  = var.gcp_zone
  physical_block_size_bytes = 4096
}
